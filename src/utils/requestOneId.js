import axios from "axios";
import store from "@/store";

const service = axios.create({
    baseURL: "https://my.mehnat.uz/",
    timeout: 10000,
});

service.interceptors.request.use(
    (config) => {
        store.dispatch("loader/increase");
        return config;
    },
    (error) => {
        console.log(error); // for debug
        return Promise.reject(error);
    }
);

service.interceptors.response.use(
    (response) => {
        store.dispatch("loader/decrease");
        const res = response.data;
        if (response.status !== 200) {
            console.log("service.interceptors.response.use response", response);
            return Promise.reject(new Error(res.message || "Error"));
        } else {
            return res;
        }
    },
    (error) => {
        store.dispatch("loader/decrease");
        console.log(error); // for debug
        return Promise.reject(error);
    }
);

export default service;