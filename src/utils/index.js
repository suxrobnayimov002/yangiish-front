export function getTotal(data, field) {
    let total = 0;
    if (data && data.length && isset(data[0][field])) {
        data.forEach((item) => {
            total += Number(item[field]);
        });
    }
    return total;
}

export function getValBySoato(data, item, field) {
    let model = data.find((element) => element.soatocode == item.soatocode);
    return model[field];
}

export function isset(variable) {
    return typeof variable != "undefined" && variable !== null;
}