import Cookies from "js-cookie";

const AccessTokenKey = "gateway-mehnat_web_access_token";
const RefreshTokenKey = "gateway-mehnat_web_refresh_token";

//set functions
export function setAccessToken(token) {
    return localStorage.setItem(AccessTokenKey, token);
}
export function setRefreshToken(token) {
    return Cookies.set(RefreshTokenKey, token);
}
//remove functions
export function removeAccessToken() {
    return localStorage.removeItem(AccessTokenKey);
}
export function removeRefreshToken() {
    return Cookies.remove(RefreshTokenKey);
}
// get functions
export function getAccessToken() {
    return localStorage.getItem(AccessTokenKey);
}
export function getRefreshToken() {
    return Cookies.get(RefreshTokenKey);
}
