import request from "@/utils/request";
import requestOneId from "@/utils/requestOneId";
import querystring from "querystring";
const oneidAuthParams = {
    grant_type: "password",
    client_id: "14",
    client_secret: "qxNCn7bLav0vFyrjE7b98hZgfiHByMyuWC3wGxm4",
    username: "oneid",
    scope: "",
};

export function login(data) {
    return request({
        url: "login",
        method: "POST",
        data,
    });
}
export function logout() {
    return request({
        url: "logout",
        method: "get",
    });
}

export function refresh(refresh) {
    return request({
        url: "refresh",
        method: "POST",
        data: refresh,
    });
}
export function getInfo(params) {
    return request({
        url: "auth/get-info",
        method: "POST",
        params: params,
    });
}

export function loginViaOneId(code) {
    let params = {
        grant_type: "one_authorization_code",
        client_id: "mehnat_uz",
        client_secret: "rQ12LK11rOKuFtHHPwL21a==",
        code: decodeURIComponent(code),
    };
    return requestOneId({
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        url: "/oneidnew/",
        method: "post",
        data: querystring.stringify(params),
    });
}
export function apiLogin(token) {
    oneidAuthParams["password"] = token;
    return request({
        url: "/oauth/token",
        method: "post",
        data: oneidAuthParams,
    });
}