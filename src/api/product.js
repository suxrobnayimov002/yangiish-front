import request from "@/utils/request";

export function inventory() {
    return request({
        url: "products/inventory",
        method: "GET",
    });
}