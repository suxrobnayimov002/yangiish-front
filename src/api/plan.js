import request from "@/utils/request";

export function reportBySoato(query) {
    return request({
        url: "plans/report-by-soato",
        method: "GET",
        params: query,
    });
}
export function fullReportBySoato(query) {
    return request({
        url: "reports/get-full-info-report",
        method: "GET",
        params: query,
    });
}
export function reportYattBySoato(query) {
    return request({
        url: "plans/report-yatt-by-soato",
        method: "GET",
        params: query,
    });
}
export function reportInvestmentBySoato(query) {
    return request({
        url: "plans/report-investment-by-soato",
        method: "GET",
        params: query,
    });
}
export function reportCompaniesMonthlyBySoato(query) {
    return request({
        url: "plans/report-companies-monthly-by-soato",
        method: "GET",
        params: query,
    });
}

export function statisticNewPositionsByProjectId(query) {
    return request({
        url: "plans/statistic-positions-monthly-by-project",
        method: "GET",
        params: query,
    });
}

export function reportPositionsQuarterBySoato(query) {
    return request({
        url: "plans/report-positions-quarter-by-soato",
        method: "GET",
        params: query,
    });
}

export function reportPositionsQuarterTotalBySoato(query) {
    return request({
        url: "plans/report-positions-quarter-total-by-soato",
        method: "GET",
        params: query,
    });
}

export function reportCompaniesBySoato(query) {
    return request({
        url: "reports/companies-report-by-soato",
        method: "GET",
        params: query,
    });
}
export function getRegionReportPositionsFirst(query) {
    return request({
        url: "reports/region-report-by-soato-first",
        method: "GET",
        params: query,
    });
}
export function getRegionReportPositionsSecond(query) {
    return request({
        url: "reports/region-report-by-soato-second",
        method: "GET",
        params: query,
    });
}
export function getCompaniesFullByMonthly(query) {
    return request({
        url: "plans/get-full-companies-by-monthly",
        method: "GET",
        params: query,
    });
}

export function hasNotInfoGnk() {
    return request({
        url: "plans/gnk/has-not-info",
        method: "GET",
    });
}
export function getDublicateCompanies() {
    return request({
        url: "plans/get-duplicate-companies",
        method: "GET",
    });
}
export function getCompaniesVacancyConut(query) {
    return request({
        url: "plans/get-companies-vacancies-by-soato",
        method: "GET",
        params: query,
    });
}
export function getCompareCurrentMonth(query) {
    return request({
        url: "plans/get-compare-companies-by-soato",
        method: "GET",
        params: query,
    });
}