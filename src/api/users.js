
import request from "@/utils/request";

export function inventory(query) {
    return request({
        url: "users/inventory",
        method: "GET",
        params: query
    });
}
