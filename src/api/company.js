import request from "@/utils/request";

export function index(params) {
    return request({
        url: "companies",
        method: "GET",
        params: params,
    });
}
export function inventory(params) {
    return request({
        url: "companies",
        method: "GET",
        params: params,
    });
}
export function getByTin(data) {
    return request({
        url: "companies/get-by-tin",
        method: "POST",
        data,
    });
}

export function hasNotEnst(data) {
    return request({
        url: "companies/get-has-not-enst",
        method: "POST",
        data,
    });
}