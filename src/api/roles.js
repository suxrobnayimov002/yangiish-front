import request from "@/utils/request";

export function permissionParents(query) {
    return request({
        url: "permissions/parents",
        method: "GET",
        params: query,
    });
}
export function inventory(query) {
    return request({
        url: "roles/inventory",
        method: "GET",
        params: query,
    });
}