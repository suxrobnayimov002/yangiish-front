import request from "@/utils/request";

export function store(data) {
    return request({
        url: "yatt-plans/store-yatt",
        method: "POST",
        data,
    });
}