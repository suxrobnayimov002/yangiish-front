import request from "@/utils/request";

export function store(data) {
    return request({
        url: "yatt-plans/store-craftsman",
        method: "POST",
        data,
    });
}