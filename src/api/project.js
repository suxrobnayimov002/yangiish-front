import request from "@/utils/request";

export function parents() {
    return request({
        url: "projects/parents",
        method: "GET",
    });
}