import request from "@/utils/request";

export function store(data) {
    return request({
        url: "yatt-plans/store-yakk",
        method: "POST",
        data,
    });
}