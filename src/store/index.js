import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import global from "./modules/global";
import auth from "./modules/auth";
import user from "./modules/user";
import company from "./modules/company";
import company_not_enst from "./modules/company_not_enst";
import plan from "./modules/plan";
import project from "./modules/project";
import product from "./modules/product";
import region_plan from "./modules/region_plan";
import yatt_plan from "./modules/yatt_plan";
import yakk_plan from "./modules/yakk_plan";
import craftsman_plan from "./modules/craftsman_plan";

const store = new Vuex.Store({
    modules: {
        global,
        auth,
        user,
        plan,
        project,
        company,
        product,
        region_plan,
        yatt_plan,
        yakk_plan,
        craftsman_plan,
        company_not_enst,
    },
});
export default store;