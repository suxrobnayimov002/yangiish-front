export const links = {
    store: {
        name: "YattPlanStore",
        path: "/yatt_plans/create",
        permission: "yatt_plans.store",
    },
    update: {
        name: "YattPlanUpdate",
        path: "/yatt_plans/update",
        permission: "yatt_plans.update",
    },
    delete: {
        name: "YattPlanDelete",
        permission: "yatt_plans.delete",
    },
};