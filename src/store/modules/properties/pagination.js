export const pagination = {
    page: 1,
    limit: 100,
    total: 0,
    sort: "id desc",
};