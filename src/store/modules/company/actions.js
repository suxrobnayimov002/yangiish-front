import * as crud from "@/utils/api/crud";
import * as types from "./properties/mutation_types";

import { inventory, getByTin, hasNotEnst } from "@/api/company";
export const actions = {
    list({ commit }, query) {
        let url = "companies/";
        return crud.list({
                commit,
            },
            types.namespace,
            url,
            query
        );
    },
    inventory({ commit }, query) {
        return new Promise((resolve, reject) => {
            inventory(query)
                .then((res) => {
                    commit(types.SET_ITEMS, res.result);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    store({ commit }, data) {
        let url = "companies/";
        return crud.store({
                commit,
            },
            types.namespace,
            url,
            data
        );
    },
    show({ dispatch, commit }, data) {
        let url = "companies/" + data.id;
        return crud.show({
                dispatch,
                commit,
            },
            types.namespace,
            url
        );
    },
    update({ commit }, data) {
        let url = "companies/" + data.id;
        return crud.update({
                commit,
            },
            types.namespace,
            url,
            data
        );
    },
    delete({ commit }, id) {
        let url = "companies/" + id;
        return crud.destroy({
                commit,
            },
            types.namespace,
            url
        );
    },

    getByTin({ commit }, data) {
        return new Promise((resolve, reject) => {
            getByTin(data)
                .then((response) => {
                    const data = response;
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    emptyList({ commit }) {
        commit(types.EMPTY_ITEMS);
    },
    updateSort({ commit }, sort) {
        commit(types.UPDATE_SORT, sort);
    },
    updateFilter({ commit }, filter) {
        commit(types.UPDATE_FILTER, JSON.parse(JSON.stringify(filter)));
    },
    updateColumn({ commit }, column) {
        commit(types.UPDATE_COLUMN, column);
    },
    updatePagination({ commit }, pagination) {
        commit(types.UPDATE_PAGINATION, pagination);
    },
    resetModel({ commit }) {
        commit(types.RESET);
    },
};