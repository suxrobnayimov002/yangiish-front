export const getters = {
    ITEMS: (state) => state.items,
    ITEM: (state) => state.item,
    FILTER: (state) => state.filter,
    SORT: (state) => state.sort,
    COLUMNS: (state) => state.columns,
    PAGINATION: (state) => state.pagination,
    LINKS: (state) => state.links,
    RULES: (state) => state.rules,
    FORM: (state) => {
        return {
            tin: state.item.tin,
            name: state.item.name,
            actual_soato: state.item.actual_soato,
            soogu: state.item.soogu,
            oked: state.item.oked,
            date_start: state.item.date_start,
            date_enst_reg: state.item.date_enst_reg,
        };
    },
};