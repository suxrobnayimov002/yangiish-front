export const getters = {
    ITEMS: (state) => state.items,
    ITEM: (state) => state.item,
    FILTER: (state) => state.filter,
    SORT: (state) => state.sort,
    COLUMNS: (state) => state.columns,
    PAGINATION: (state) => state.pagination,
    LINKS: (state) => state.links,
    RULES: (state) => state.rules,
    FORM: (state) => {
        return {
            id: state.item.id,
            name: state.item.name,
            parent: state.item.parent,
            parent_id: state.item.parent_id,
        };
    },
};