export const links = {
    store: {
        name: "ProjectStore",
        path: "/projects/create",
        permission: "projects.store",
    },
    update: {
        name: "ProjectUpdate",
        path: "/projects/update",
        permission: "projects.update",
    },
    // delete: {
    //     name: "ProjectDelete",
    //     permission: "projects.delete",
    // },
};