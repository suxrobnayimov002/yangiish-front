export const columns = {
    id: {
        show: true,
        title: "№",
        sortable: true,
        column: "id",
        length: 120,
    },
    name: {
        show: true,
        title: "Номи",
        sortable: true,
        column: "name",
        length: 120,
    },
    parent: {
        show: true,
        title: " Бош лойиҳа",
        sortable: true,
        column: "parent",
        length: 120,
    },
    settings: {
        show: true,
        title: "Aмаллар",
        sortable: true,
        column: "settings",
        length: 120,
    },
};