import * as crud from "@/utils/api/crud";
import * as types from "./properties/mutation_types";
import { parents } from "@/api/project";
export const actions = {
    list({ commit }, query) {
        let url = "projects/";
        return crud.list({
                commit,
            },
            types.namespace,
            url,
            query
        );
    },
    parents({ commit }) {
        return new Promise((resolve, reject) => {
            parents()
                .then((res) => {
                    commit(types.SET_ITEMS, res.data);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    store({ commit }, data) {
        let url = "projects/";
        return crud.store({
                commit,
            },
            types.namespace,
            url,
            data
        );
    },
    show({ dispatch, commit }, data) {
        let url = "projects/" + data.id;
        return crud.show({
                dispatch,
                commit,
            },
            types.namespace,
            url
        );
    },
    update({ commit }, data) {
        let url = "projects/" + data.id;
        return crud.update({
                commit,
            },
            types.namespace,
            url,
            data
        );
    },
    delete({ commit }, id) {
        let url = "projects/" + id;
        return crud.destroy({
                commit,
            },
            types.namespace,
            url
        );
    },
    emptyList({ commit }) {
        commit(types.EMPTY_ITEMS);
    },
    updateSort({ commit }, sort) {
        commit(types.UPDATE_SORT, sort);
    },
    updateFilter({ commit }, filter) {
        commit(types.UPDATE_FILTER, JSON.parse(JSON.stringify(filter)));
    },
    updateColumn({ commit }, column) {
        commit(types.UPDATE_COLUMN, column);
    },
    updatePagination({ commit }, pagination) {
        commit(types.UPDATE_PAGINATION, pagination);
    },
    resetModel({ commit }) {
        commit(types.RESET);
    },
};