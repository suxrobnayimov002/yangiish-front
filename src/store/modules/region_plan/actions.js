import * as crud from "@/utils/api/crud";
import * as types from "./properties/mutation_types";
export const actions = {
	list({ commit }, query) {
		let url = "region-plans/";
		return crud.list({
			  commit,
			},
			types.namespace,
			url,
			query
		);
	},
	inventory({ commit }, query) {
		return new Promise((resolve, reject) => {
			inventory(query)
				.then((res) => {
					commit(types.SET_ITEMS, res.data);
					resolve(res);
				})
				.catch((error) => {
						reject(error);
				});
		});
	},
	store({ commit }, data) {
		let url = "region-plans/";
		return crud.store({
				commit,
			},
			types.namespace,
			url,
			data
		);
	},
	show({ dispatch, commit }, data) {
		let url = "region-plans/" + data.id;
		return crud.show({
				dispatch,
				commit,
			},
			types.namespace,
			url
		);
	},
	update({ commit }, data) {
		let url = "region-plans/" + data.id;
		return crud.update({
				commit,
			},
			types.namespace,
			url,
			data
		);
	},
	delete({ commit }, id) {
		let url = "region-plans/" + id;
		return crud.destroy({
				commit,
			},
			types.namespace,
			url
		);
	},
	emptyList({ commit }) {
		commit(types.EMPTY_ITEMS);
	},
	updateSort({ commit }, sort) {
		commit(types.UPDATE_SORT, sort);
	},
	updateFilter({ commit }, filter) {
		commit(types.UPDATE_FILTER, JSON.parse(JSON.stringify(filter)));
	},
	updateColumn({ commit }, column) {
		commit(types.UPDATE_COLUMN, column);
	},
	updatePagination({ commit }, pagination) {
		commit(types.UPDATE_PAGINATION, pagination);
	},
	resetModel({ commit }) {
		commit(types.RESET);
	},
};