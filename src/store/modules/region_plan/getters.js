export const getters = {
	ITEMS: (state) => state.items,
	ITEM: (state) => state.item,
	FILTER: (state) => state.filter,
	SORT: (state) => state.sort,
	COLUMNS: (state) => state.columns,
	PAGINATION: (state) => state.pagination,
	LINKS: (state) => state.links,
	RULES: (state) => state.rules,
	MONTHS: (state) => state.months,
	FORM: (state) => {
		return {
			id: state.item.id,
			soato: state.item.soato,
			project_id: state.item.project_id,
			project: state.item.project,
			plan: state.item.plan,
			year: state.item.year,
		};
	},
};