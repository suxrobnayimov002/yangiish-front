export const links = {
    store: {
        name: "RegionPlanStore",
        path: "/region_plans/create",
        permission: "region_plans.store",
    },
    update: {
        name: "RegionPlanUpdate",
        path: "/region_plans/update",
        permission: "region_plans.update",
    },
    delete: {
        name: "RegionPlanDelete",
        permission: "region_plans.delete",
    },
};