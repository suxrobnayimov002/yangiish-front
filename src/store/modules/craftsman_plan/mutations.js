import * as types from "./properties/mutation_types";
import { regions } from "../../../utils/regions";

import { model } from "./properties/model";

export const mutations = {
    [types.SET_ERROR](state, error) {
        Object.assign(state, { error });
    },
    [types.SET_ITEMS](state, items) {
        var arr = [];

        let all_regions = regions;
        let region = null;
        items.forEach((item) => {
            let first_soato = item.soato ? item.soato : "1726";
            if (first_soato.length > 4) {
                all_regions = regions.find(
                    (region) => region.soato == first_soato.slice(0, 4)
                ).under;
            } else {
                all_regions = regions;
            }
            region = null;
            region = all_regions.find((region) => region.soato == item.soato);
            if (region) {
                item.name = region.name_uz_cl;
            }
        });
        state.items = items;
    },
    [types.SET_ITEM](state, item) {
        state.item = item;
    },
    [types.EMPTY_ITEMS](state) {
        state.items = [];
    },
    [types.UPDATE_FILTER]: (state, filter) => (state.filter = filter),

    [types.SET_PAGINATION]: (state, pagination) =>
        (state.pagination = pagination),

    [types.UPDATE_COLUMN]: (state, obj) => {
        state.columns[obj.key].show = obj.value;
    },
    [types.UPDATE_SORT]: (state, sort) => {
        state.sort[sort.column] = sort.order;
    },
    [types.UPDATE_PAGINATION]: (state, pagination) => {
        state.pagination[pagination.key] = pagination.value;
    },
    [types.RESET]: (state) => {
        state.item = JSON.parse(JSON.stringify(model));
    },
};