export const links = {
    store: {
        name: "CraftsmanPlanStore",
        path: "/craftsman_plans/create",
        permission: "craftsman_plans.store",
    },
    update: {
        name: "CraftsmanPlanUpdate",
        path: "/craftsman_plans/update",
        permission: "craftsman_plans.update",
    },
    delete: {
        name: "CraftsmanPlanDelete",
        permission: "craftsman_plans.delete",
    },
};