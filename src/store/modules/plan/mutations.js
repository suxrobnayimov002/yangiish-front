import * as types from "./properties/mutation_types";
import { regions } from "../../../utils/regions";
import { model } from "./properties/model";

export const mutations = {
    [types.SET_ERROR](state, error) {
        Object.assign(state, { error });
    },
    [types.SET_ITEMS](state, items) {
        state.items = items;
    },
    [types.SET_ITEM](state, item) {
        state.item = item;
    },
    [types.EMPTY_ITEMS](state) {
        state.items = [];
    },
    [types.UPDATE_FILTER]: (state, filter) => (state.filter = filter),

    [types.SET_PAGINATION]: (state, pagination) =>
        (state.pagination = pagination),

    [types.UPDATE_COLUMN]: (state, obj) => {
        state.columns[obj.key].show = obj.value;
    },
    [types.UPDATE_SORT]: (state, sort) => {
        state.sort[sort.column] = sort.order;
    },
    [types.UPDATE_PAGINATION]: (state, pagination) => {
        state.pagination[pagination.key] = pagination.value;
    },
    [types.RESET]: (state) => {
        state.item = JSON.parse(JSON.stringify(model));
    },

    SET_REPORT_PLAN: (state, data) => {
        var arr = [];
        let first_soato = data[0].soatocode;
        let all_regions = regions;
        if (first_soato.length > 4) {
            all_regions = regions.find(
                (region) => region.soato == first_soato.slice(0, 4)
            ).under;
        }
        all_regions.forEach((region) => {
            let model = data.find((item) => item.soatocode == region.soato);
            if (model) {
                model.region_name = region.name_uz_cl;
                model.soato = region.soato;
                arr.push(model);
            }
        });
        state.record_plan = arr;
    },
    SET_REPORT_PLAN_TOTAL: (state, data) => {
        state.record_plan_total = data;
    },
    SET_QUARTER_REPORT_POSITIONS_TOTAL: (state, data) => {
        state.quarter_record_positions_total = data;
    },
    SET_QUARTER_REPORT_TOTAL_FULL_PERSONS: (state, data) => {
        state.quarter_record_total_full_persons = data;
    },
    SET_REPORT_COMPANY_PLAN: (state, data) => {
        state.record_plan = data;
    },

    SET_QUARTER_REPORT_POSITIONS: (state, data) => {
        var arr = [];
        if (data.length > 0) {
            let first_soato = data[0].soatocode;
            let all_regions = regions;
            if (first_soato.length > 4) {
                all_regions = regions.find(
                    (region) => region.soato == first_soato.slice(0, 4)
                ).under;
            }
            all_regions.forEach((region) => {
                let model = data.find((item) => item.soatocode == region.soato);
                if (model) {
                    model.region_name = region.name_uz_cl;
                    model.soato = region.soato;
                    arr.push(model);
                }
            });
        }
        state.quarter_record_positions = arr;
    },
    SET_STATISTIC_COMPANY_PLAN: (state, data) => {
        state.statistic_plan.gnk = [];
        state.statistic_plan.plan = [];

        if (data.length > 0) {
            let first_soato = data[0].soatocode;
            let all_regions = regions;
            if (first_soato.length > 4) {
                all_regions = regions.find(
                    (region) => region.soato == first_soato.slice(0, 4)
                ).under;
            }

            all_regions.forEach((region) => {
                let model = data.find((item) => item.soatocode == region.soato);
                if (model) {
                    state.statistic_plan.gnk.push(model.gnk > 0 ? model.gnk : 0);
                    state.statistic_plan.plan.push(model.plan > 0 ? model.plan : 0);
                } else {
                    state.statistic_plan.gnk.push(0);
                    state.statistic_plan.plan.push(0);
                }
            });
        }
    },
};