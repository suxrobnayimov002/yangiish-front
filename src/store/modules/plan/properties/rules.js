export const rules = {
    company_tin: [{
        required: true,
        message: "Тўлдириш мажбурий бўлган майдон.",
        trigger: "change",
    }, ],
    positions_plan: [{
        required: true,
        message: "Тўлдириш мажбурий бўлган майдон.",
        trigger: "change",
    }, ],
    project_id: [{
        required: true,
        message: "Тўлдириш мажбурий бўлган майдон.",
        trigger: "change",
    }, ],
    year: [{
        required: true,
        message: "Тўлдириш мажбурий бўлган майдон.",
        trigger: "change",
    }, ],
    month: [{
        required: true,
        message: "Тўлдириш мажбурий бўлган майдон.",
        trigger: "change",
    }, ],
};