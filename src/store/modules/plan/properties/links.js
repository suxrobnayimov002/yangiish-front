export const links = {
    store: {
        name: "PlanStore",
        path: "/plans/create",
        permission: "plans.store",
    },
    update: {
        name: "PlanUpdate",
        path: "/plans/update",
        permission: "plans.update",
    },
    delete: {
        name: "PlanDelete",
        permission: "plans.delete",
    },
};