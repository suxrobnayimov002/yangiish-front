export const model = {
    id: null,
    company_tin: "",
    positions_plan: null,
    year: null,
    month: null,
    company: null,
    project_id: null,
    product_id: null,
    product_volume: null,
    projects_count: 1,
};