export const columns = {
    id: {
        show: true,
        title: "№",
        sortable: true,
        column: "id",
        length: 120,
        filter_index: "none",
    },
    company_tin: {
        show: true,
        title: "Корхона ИНН",
        sortable: true,
        column: "company_tin",
        length: 120,
        filter_index: 0,
    },
    project: {
        show: true,
        title: "Лойиҳа",
        sortable: true,
        column: "project",
        length: 120,
        filter_index: "none",
    },
    positions_plan: {
        show: true,
        title: "Иш ўринлари сони",
        sortable: true,
        column: "positions_plan",
        length: 120,
        filter_index: "none",
    },
    year: {
        show: true,
        title: "Йил",
        sortable: true,
        column: "year",
        length: 120,
        filter_index: 1,
    },
    month: {
        show: true,
        title: "Ой",
        sortable: true,
        column: "month",
        length: 120,
        filter_index: 2,
    },
    product: {
        show: true,
        title: "Ишлаб чиқариладиган маҳсулот",
        sortable: true,
        column: "project",
        length: 120,
        filter_index: "none",
    },

    settings: {
        show: true,
        title: "Aмаллар",
        sortable: true,
        column: "settings",
        length: 120,
        filter_index: "button",
    },
};