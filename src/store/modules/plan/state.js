import { model } from "./properties/model";
import { columns } from "./properties/columns";
import { filter } from "./properties/filter";
import { rules } from "./properties/rules";
import { pagination } from "./../properties/pagination";
import { sort } from "./../properties/sort";
import { links } from "./properties/links";
export const state = {
    items: [],
    item: JSON.parse(JSON.stringify(model)),
    columns: columns,
    filter: JSON.parse(JSON.stringify(filter)),
    rules: rules,
    sort: JSON.parse(JSON.stringify(sort)),
    links: links,
    pagination: JSON.parse(JSON.stringify(pagination)),
    record_plan: [],
    record_plan_total: null,
    statistic_plan: {
        enst: [],
        plan: [],
    },
    quarter_record_positions: null,
    quarter_record_positions_total: [],
    quarter_record_total_full_persons: [],
    months: [{
            id: 1,
            name: "Январ",
        },
        {
            id: 2,
            name: "Феврал",
        },
        {
            id: 3,
            name: "Март",
        },
        {
            id: 4,
            name: "Aпрел",
        },
        {
            id: 5,
            name: "Май",
        },
        {
            id: 6,
            name: "Июн",
        },
        {
            id: 7,
            name: "Июл",
        },
        {
            id: 8,
            name: "Aвгуст",
        },
        {
            id: 9,
            name: "Cентябр",
        },
        {
            id: 10,
            name: "Октябр",
        },
        {
            id: 11,
            name: "Ноябр",
        },
        {
            id: 12,
            name: "Декабр",
        },
    ],
};