export const getters = {
    ITEMS: (state) => state.items,
    ITEM: (state) => state.item,
    FILTER: (state) => state.filter,
    SORT: (state) => state.sort,
    COLUMNS: (state) => state.columns,
    PAGINATION: (state) => state.pagination,
    LINKS: (state) => state.links,
    RULES: (state) => state.rules,
    MONTHS: (state) => state.months,
    STATISTIC_PLAN: (state) => state.statistic_plan,
    RECORD_PLAN: (state) => state.record_plan,
    RECORD_PLAN_TOTAL: (state) => state.record_plan_total,
    QUARTER_RECORD_POSITIONS: (state) => state.quarter_record_positions,
    QUARTER_RECORD_POSITIONS_TOTAL: (state) =>
        state.quarter_record_positions_total,
    QUARTER_RECORD_TOTAL_FULL_PERSONS: (state) =>
        state.quarter_record_total_full_persons,
    FORM: (state) => {
        return {
            id: state.item.id,
            company_tin: state.item.company_tin,
            positions_plan: state.item.positions_plan,
            year: state.item.year,
            project_id: state.item.project_id,
            product_id: state.item.product_id,
            product_volume: state.item.product_volume,
            month: state.item.month,
            company: state.item.company,
            projects_count: state.item.projects_count,
        };
    },
};