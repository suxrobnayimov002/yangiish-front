import * as crud from "@/utils/api/crud";
import * as types from "./properties/mutation_types";
import {
    reportBySoato,
    reportYattBySoato,
    reportInvestmentBySoato,
    reportCompaniesMonthlyBySoato,
    statisticNewPositionsByProjectId,
    reportPositionsQuarterBySoato,
    reportPositionsQuarterTotalBySoato,
    reportCompaniesBySoato,
    getRegionReportPositionsFirst,
    getRegionReportPositionsSecond,
    hasNotInfoGnk,
    getDublicateCompanies,
    getCompaniesFullByMonthly,
    getCompareCurrentMonth,
    getCompaniesVacancyConut,
    fullReportBySoato,
} from "@/api/plan";
export const actions = {
    list({ commit }, query) {
        let url = "plans/";
        return crud.list({
                commit,
            },
            types.namespace,
            url,
            query
        );
    },
    store({ commit }, data) {
        let url = "plans/";
        return crud.store({
                commit,
            },
            types.namespace,
            url,
            data
        );
    },
    show({ dispatch, commit }, data) {
        let url = "plans/" + data.id;
        return crud.show({
                dispatch,
                commit,
            },
            types.namespace,
            url
        );
    },
    update({ commit }, data) {
        let url = "plans/" + data.id;
        return crud.update({
                commit,
            },
            types.namespace,
            url,
            data
        );
    },
    delete({ commit }, id) {
        let url = "plans/" + id;
        return crud.destroy({
                commit,
            },
            types.namespace,
            url
        );
    },
    emptyList({ commit }) {
        commit(types.EMPTY_ITEMS);
    },
    updateSort({ commit }, sort) {
        commit(types.UPDATE_SORT, sort);
    },
    updateFilter({ commit }, filter) {
        commit(types.UPDATE_FILTER, JSON.parse(JSON.stringify(filter)));
    },
    updateColumn({ commit }, column) {
        commit(types.UPDATE_COLUMN, column);
    },
    updatePagination({ commit }, pagination) {
        commit(types.UPDATE_PAGINATION, pagination);
    },
    resetModel({ commit }) {
        commit(types.RESET);
    },

    reportBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportBySoato(query)
                .then((res) => {
                    commit("SET_REPORT_PLAN", res.data.data);
                    commit("SET_REPORT_PLAN_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    fullReportBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            fullReportBySoato(query)
                .then((res) => {
                    console.log(res);
                    commit("SET_REPORT_PLAN", res.data.data);
                    commit("SET_REPORT_PLAN_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    reportYattBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportYattBySoato(query)
                .then((res) => {
                    commit("SET_REPORT_PLAN", res.data);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    reportInvestmentBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportInvestmentBySoato(query)
                .then((res) => {
                    commit("SET_REPORT_COMPANY_PLAN", res.data);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    reportCompaniesMonthlyBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportCompaniesMonthlyBySoato(query)
                .then((res) => {
                    commit("SET_REPORT_COMPANY_PLAN", res.data.data);
                    commit("SET_REPORT_PLAN_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    statisticNewPositionsByProjectId({ commit }, query) {
        return new Promise((resolve, reject) => {
            statisticNewPositionsByProjectId(query)
                .then((res) => {
                    commit("SET_STATISTIC_COMPANY_PLAN", res.data);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    reportPositionsQuarterBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportPositionsQuarterBySoato(query)
                .then((res) => {
                    commit("SET_QUARTER_REPORT_POSITIONS", res.data.data);
                    commit("SET_QUARTER_REPORT_POSITIONS_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    reportPositionsQuarterTotalBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportPositionsQuarterTotalBySoato(query)
                .then((res) => {
                    commit("SET_QUARTER_REPORT_TOTAL_FULL_PERSONS", res.data);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    reportCompaniesBySoato({ commit }, query) {
        return new Promise((resolve, reject) => {
            reportCompaniesBySoato(query)
                .then((res) => {
                    commit("SET_REPORT_COMPANY_PLAN", res.data.data);
                    commit("SET_REPORT_PLAN_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    getRegionReportPositionsFirst({ commit }, query) {
        return new Promise((resolve, reject) => {
            getRegionReportPositionsFirst(query)
                .then((res) => {
                    commit("SET_QUARTER_REPORT_POSITIONS", res.data.data);
                    commit("SET_QUARTER_REPORT_POSITIONS_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    getRegionReportPositionsSecond({ commit }, query) {
        return new Promise((resolve, reject) => {
            getRegionReportPositionsSecond(query)
                .then((res) => {
                    commit("SET_QUARTER_REPORT_POSITIONS", res.data.data);
                    commit("SET_QUARTER_REPORT_POSITIONS_TOTAL", res.data.total);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    hasNotInfoGnk({ commit }) {
        return new Promise((resolve, reject) => {
            hasNotInfoGnk()
                .then((res) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    getDublicateCompanies({ commit }) {
        return new Promise((resolve, reject) => {
            getDublicateCompanies()
                .then((res) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    getCompaniesFullByMonthly({ commit }, query) {
        return new Promise((resolve, reject) => {
            getCompaniesFullByMonthly(query)
                .then((res) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    getCompaniesVacancyConut({ commit }, query) {
        return new Promise((resolve, reject) => {
            getCompaniesVacancyConut(query)
                .then((res) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    getCompareCurrentMonth({ commit }, query) {
        return new Promise((resolve, reject) => {
            getCompareCurrentMonth(query)
                .then((res) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
};