export const links = {
    store: {
        name: "ProductStore",
        path: "/products/create",
        permission: "products.store",
    },
    update: {
        name: "ProductUpdate",
        path: "/products/update",
        permission: "products.update",
    },
    delete: {
        name: "ProductDelete",
        permission: "products.delete",
    },
};