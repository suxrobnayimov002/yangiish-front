export const links = {
    store: {
        name: "YakkPlanStore",
        path: "/yakk_plans/create",
        permission: "yakk_plans.store",
    },
    update: {
        name: "YakkPlanUpdate",
        path: "/yakk_plans/update",
        permission: "yakk_plans.update",
    },
    delete: {
        name: "YakkPlanDelete",
        permission: "yakk_plans.delete",
    },
};