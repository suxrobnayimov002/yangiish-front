import * as crud from "@/utils/api/crud";
import * as types from "./properties/mutation_types";

import { hasNotEnst } from "@/api/company";
export const actions = {
    hasNotEnst({ commit }, data) {
        return new Promise((resolve, reject) => {
            hasNotEnst(data)
                .then((res) => {
                    commit(types.SET_ITEMS, res.data.data);
                    commit(types.UPDATE_PAGINATION, {
                        key: "total",
                        value: res.data.total,
                    });
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    emptyList({ commit }) {
        commit(types.EMPTY_ITEMS);
    },
    updateSort({ commit }, sort) {
        commit(types.UPDATE_SORT, sort);
    },
    updateFilter({ commit }, filter) {
        commit(types.UPDATE_FILTER, JSON.parse(JSON.stringify(filter)));
    },
    updateColumn({ commit }, column) {
        commit(types.UPDATE_COLUMN, column);
    },
    updatePagination({ commit }, pagination) {
        commit(types.UPDATE_PAGINATION, pagination);
    },
    resetModel({ commit }) {
        commit(types.RESET);
    },
};