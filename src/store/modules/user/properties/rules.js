var validateRule = (rule, value, callback) => {
    if (value && value.length < 6) {
        callback(
            new Error("Логин қиймати камида 6 та белгидан иборат бўлиши кера!")
        );
    } else {
        callback();
    }
};
export const rules = {
    name: [{
        required: true,
        message: "Фойдаланувчи номи киритилиши шарт.",
        trigger: "change",
    }, ],
    username: [{
            required: true,
            message: "Фойдаланувчи логини киритилиши шарт.",
            trigger: "change",
        },
        { validator: validateRule, trigger: "blur" },
    ],
    surname: [{
        required: true,
        message: "Фойдаланувчи номи киритилиши шарт.",
        trigger: "change",
    }, ],
    type: [{
        required: true,
        message: "Тип киритилиши шарт.",
        trigger: "change",
    }, ],
    soato: [{
        required: true,
        message: "Соато киритилиши шарт.",
        trigger: "change",
    }, ],
    status: [{
        required: true,
        message: "Статус танланиши шарт.",
        trigger: "change",
    }, ],
};