export const links = {
    store: {
        name: "UserStore",
        path: "/users/create",
        permission: "user.store",
    },
    // show: {
    //     name: 'UserShow',
    //     key: 'id',
    //     path: '/users/show/:id',
    //     permission: 'users.show',
    // },
    update: {
        name: "UserUpdate",
        key: "id",
        path: "/users/update/:id",
        permission: "users.update",
    },
    delete: {
        name: "Delete",
        permission: "user.delete",
    },
};