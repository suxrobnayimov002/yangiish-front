import {
    login,
    logout,
    refresh,
    getInfo,
    loginViaOneId,
    apiLogin,
} from "@/api/auth";
import * as types from "./properties/mutation_types";
import {
    setAccessToken,
    removeAccessToken,
    getAccessToken,
    getRefreshToken,
} from "@/utils/auth";
export const actions = {
    login({ commit }, data) {
        return new Promise((resolve, reject) => {
            login(data)
                .then((response) => {
                    const data = response;
                    commit(types.SET_AUTH_ACCESS_TOKEN, data.access_token);
                    setAccessToken(data.access_token);
                    //dispatch("getInfo");
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    // logout
    logout({ commit }) {
        commit(types.REMOVE_AUTH_ACCESS_TOKEN);
        removeAccessToken();
    },
    // refresh auth token
    refresh({ commit, dispatch }) {
        return new Promise((resolve, reject) => {
            const refresh_token = getRefreshToken();
            refresh({
                    refresh: refresh_token,
                })
                .then((response) => {
                    const data = response;
                    if (data.access) {
                        commit(types.SET_AUTH_ACCESS_TOKEN, data.access_token);
                        setAccessToken(data.access_token);
                        resolve(response);
                    }
                })
                .catch((err) => {
                    reject(err);
                    dispatch("resetToken");
                });
        });
    },
    resetToken({ commit }) {
        commit(types.REMOVE_AUTH_ACCESS_TOKEN);
        removeAccessToken();
    },

    checkExperesToken({ commit, dispatch }) {
        let token = getAccessToken();
        if (!token) {
            return false;
        }
        var base64Url = token.split(".")[1];
        var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        var jsonPayload = decodeURIComponent(
            atob(base64)
            .split("")
            .map(function(c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );
        if (!JSON.parse(jsonPayload).exp) {
            dispatch("resetToken");
            return false;
        }
        let time = new Date((JSON.parse(jsonPayload).exp - 1200) * 1000);
        if (time < new Date()) {
            dispatch("refresh");
        }
        // setTimeout(checkExperesToken(), 120000);
        return true;
    },
    getInfo({ commit, dispatch }) {
        return new Promise((resolve, reject) => {
            getInfo({ include: "Permissions" })
                .then((res) => {
                    commit(types.SET_AUTH_USER, res.data);
                    resolve(res);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    setToken(context, token) {
        context.commit(types.SET_AUTH_ACCESS_TOKEN, token);
        setAccessToken(token);
    },
    loginViaOneId({ commit }, code) {
        return new Promise((resolve, reject) => {
            loginViaOneId(code)
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    apiLogin({ commit }, token) {
        return new Promise((resolve, reject) => {
            apiLogin(token)
                .then((res) => {
                    let token = res.access_token;
                    commit(types.SET_AUTH_ACCESS_TOKEN, token);
                    setAccessToken(token);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
};