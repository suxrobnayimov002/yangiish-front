import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "./assets/main.css";
import Index from "@/mixins/index";
import PermissionMixin from "@/mixins/permission";
import TableIndex from "@/components/Table";
import locale from "element-ui/lib/locale";
import lang from "./assets/lang/uz-cyril-UZ";

Vue.use(ElementUI);
Vue.use(ElementUI, { locale });
locale.use(lang);
Vue.config.productionTip = false;
import VueClipboard from "vue-clipboard2";

Vue.use(VueClipboard);

Vue.mixin(Index);
Vue.mixin(PermissionMixin);

Vue.component("table-index", TableIndex);
window._ = require("lodash");
new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");