import { mapGetters, mapActions } from "vuex";
export default {
    data() {
        return {
            waiting: false,
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            data: "plan/RECORD_PLAN",
        }),
        soato() {
            return this.$route.params.soato;
        },
    },
    methods: {
        ...mapActions({
            reportBySoato: "plan/reportYattBySoato",
        }),
        getReportData() {
            this.waiting = true;
            let a_soato = this.soato ? this.soato : "17";
            this.reportBySoato({ soato: a_soato })
                .then(() => {
                    this.waiting = false;
                    this.setTotal();
                })
                .catch(() => {
                    this.waiting = false;
                });
        },
    },
    async mounted() {
        this.getReportData();
    },
};