import { mapGetters, mapActions } from "vuex";
import { regions } from "../../../utils/regions";

export default {
    data() {
        return {
            waiting: false,
            soato: this.$route.params.soato,
            report_type_name: this.$route.query.report_type_name,
            type_id: JSON.parse(this.$route.params.type_id),
            types: [{
                    value: 8,
                    label: "соғлиқни сақлаш",
                },
                {
                    value: 9,
                    label: "мактабгача таълим",
                },
                {
                    value: 10,
                    label: "халқ таълими",
                },
                {
                    value: 11,
                    label: "олий таълим",
                },
            ],
        };
    },
    computed: {
        ...mapGetters({
            data: "plan/RECORD_PLAN",
            total: "plan/RECORD_PLAN_TOTAL",
        }),
        region() {
            return regions.find((region) => region.soato == this.soato);
        },
    },
    watch: {
        type_id: {
            handler: async function(newVal, oldVal) {
                if (newVal != oldVal) {
                    this.changeReportType(newVal);
                    this.getReportCompaniesMonthlyBySoato();
                }
            },
        },
    },
    created() {
        this.getReportCompaniesMonthlyBySoato();
    },
    methods: {
        ...mapActions({
            // reportCompaniesMonthlyBySoato: "plan/reportCompaniesMonthlyBySoato",
            reportCompaniesMonthlyBySoato: "plan/reportCompaniesBySoato",
        }),
        goBack() {
            this.$router.push({
                name: "ReportSocial",
                params: { project_id: 7, soato: this.soato },
            });
        },

        getReportCompaniesMonthlyBySoato() {
            this.waiting = true;
            this.reportCompaniesMonthlyBySoato({
                    soato: this.soato,
                    project_id: this.type_id,
                })
                .then(() => {
                    this.waiting = false;
                })
                .catch(() => {
                    this.waiting = false;
                });
        },
        changeReportType(newVal) {
            let type = this.types.find((element) => element.value == newVal);
            this.report_type_name = type.label;
            this.type_id = type.value;
            this.$router.push({
                name: "ReportBySocial",
                params: { soato: this.soato, type_id: this.type_id },
                query: { report_type_name: this.report_type_name },
            });
        },
    },
};