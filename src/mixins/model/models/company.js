import { mapGetters, mapActions } from "vuex";
export default {
	data() {
		return {
			waiting: false,
			loading: false,
			loadingData: false,
			defaultProps: {
					children: "children",
					label: "name",
			},
			selected_projects: [],
		};
	},
	components: {},
	computed: {
		...mapGetters({
			rules: "company/RULES",
			item: "company/ITEM",
			columns: "company/COLUMNS",
			getForm: "company/FORM",
			projects: "project/ITEMS",
		}),
	},
	watch: {
		"form.tin": {
			handler: async function(newVal, oldVal) {
				if (
					newVal &&
					newVal.length == 9 &&
					newVal != oldVal &&
					!this.form.actual_soato &&
					_.isFunction(this.debouncedFetchData)
				) {
					this.debouncedFetchData();
				}
			},
			deep: true,
			immediate: true,
		},
	},
	async mounted() {},
	async created() {
		this.debouncedFetchData = _.debounce(this.getCompanyByTin, 100);
		this.list({
			model: "project",
			action_name: "parents",
			query: { include: "Children", parent_id: "null" },
		});
	},
	methods: {
		...mapActions({
			getCompanyByTinAction: "company/getByTin",
		}),
		setForm() {
			this.form = JSON.parse(JSON.stringify(this.getForm));
		},
		updateClassPositions(data) {
			this.$refs.Positions.updateClassPositions(data);
		},
		getCompanyByTin() {
			this.waiting = true;
			this.getCompanyByTinAction({ tin: this.form.tin })
				.then((res) => {
					this.waiting = false;
					this.form = res.data;
				})
				.catch((err) => {
				  this.waiting = false;
					this.$notify({
							title: "Error",
							message: "Mаълумотлари келмади.",
							type: "error",
					});
				});
		},
		resetForm() {
			this.resetModel({ model: "company" });
			this.setForm();
		},
		loadItem() {
			this.waiting = true;

			this.show({
				model: "company",
				id: this.$route.params.tin,
			})
			.then((res) => {
				this.waiting = false;
				let checked_projects = res.data.projects;
				for (let key in checked_projects) {
					this.selected_projects.push(checked_projects[key].id);
				}
				this.$refs.tree.setCheckedKeys([6]);
				this.form = JSON.parse(JSON.stringify(this.item));
			})
			.catch(() => {
				this.waiting = false;

				this.$notify({
					title: "Error",
					message: "Ҳуқуқлар маълумотлари келмади.",
					type: "error",
				});
			});
		},
		onSave() {
			this.$refs["form"].validate((valid) => {
				let checked_projects = this.$refs.tree.getCheckedNodes();
				let role_projects = [];
				for (let key in checked_projects) {
					role_projects.push(checked_projects[key].id);
				}
				this.form.id = this.form.tin;
				this.form["projects"] = role_projects;
				if (valid) {
					this.waiting = true;
					this.save(this.form)
					.then(() => {
						this.waiting = false;
						this.resetModel({ model: "company" });
						this.setForm();
						this.$refs.tree.setCheckedKeys([]);
						this.$router.push({
							name: "CompanyIndex",
						});
						this.$notify({
							title: "Success",
							message: "Маълумот сақланди.",
							type: "success",
						});
					})
					.catch((err) => {
						this.waiting = false;
						let message = "Маълумот сақлашда хатолик мавжуд.";
						if (err.data.errors.tin[0] == "The tin has already been taken.") {
							message = "Бундай маълумот мавжуд.";
						}
						this.$notify({
							title: "Error",
							message: message,
							type: "error",
						});
					});
				}
			});
		},
	},
};