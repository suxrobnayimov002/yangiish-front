import { mapGetters, mapActions } from "vuex";
export default {
    data() {
        return {
            all_projects: [],
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            projects: "project/ITEMS",
        }),
    },

    created() {
        // this.waiting = true;
        // if (!this.projects || this.projects.length === 0) {
        this.list({ model: "project", action_name: "parents" })
            .then(() => {
                this.setAllProjects();
            })
            .catch(() => {
                // this.waiting = false;
            });
        // } else {
        // this.setAllProjects();
        // }
    },
    methods: {
        setAllProjects() {
            this.projects.forEach((project) => {
                this.all_projects.push({
                    id: project.id,
                    actual_soato: project.actual_soato,
                    name: project.name,
                    is_parent: true,
                });
                if (project.children) {
                    project.children.forEach((ch_project) => {
                        this.all_projects.push({
                            id: ch_project.id,
                            actual_soato: ch_project.actual_soato,
                            name: ch_project.name,
                            is_parent: false,
                        });
                    });
                }
                // this.waiting = false;
            });
        },
    },
};