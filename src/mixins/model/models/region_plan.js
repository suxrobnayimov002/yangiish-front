import { mapGetters, mapActions } from "vuex";
import { regions } from "../../../utils/regions";

export default {
    data() {
        return {
            waiting: false,
            years: [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030],
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            rules: "region_plan/RULES",
            item: "region_plan/ITEM",
            items: "region_plan/ITEMS",
            columns: "region_plan/COLUMNS",
            getForm: "region_plan/FORM",
            projects: "project/ITEMS",
        }),
        all_regions() {
            return regions;
        },
    },
    async mounted() {
        this.list({ model: "project", action_name: "parents" }).then(() => {});
    },
    methods: {
        ...mapActions({}),
        setForm() {
            this.form = JSON.parse(JSON.stringify(this.getForm));
        },
        loadItem() {
            this.waiting = true;

            this.show({
                    model: this.model_name,
                    id: this.$route.params.id,
                    query: "",
                })
                .then(() => {
                    this.waiting = false;
                    let index = this.items.findIndex(
                        (element) => element.id == this.item.id
                    );
                    if (index > -1) {
                        this.items.splice(index, 1);
                    }
                    this.form = JSON.parse(JSON.stringify(this.item));
                })
                .catch(() => {
                    this.waiting = false;

                    this.$notify({
                        title: "Error",
                        message: "Ҳуқуқлар маълумотлари келмади.",
                        type: "error",
                    });
                });
        },
        onSave() {
            this.$refs["form"].validate((valid) => {
                if (valid) {
                    this.waiting = true;
                    this.save(this.form)
                        .then(() => {
                            this.waiting = false;
                            this.resetModel({ model: "company" });
                            this.setForm();
                            this.$router.push({
                                name: "RegionPlanIndex",
                            });
                            this.$notify({
                                title: "Success",
                                message: "Маълумот сақланди.",
                                type: "success",
                            });
                        })
                        .catch((err) => {
                            this.waiting = false;
                            let message = "Маълумот сақлашда хатолик мавжуд.";
                            if (err.status == 409) {
                                message = "Бундай маълумот мавжуд.";
                            }
                            this.$notify({
                                title: "Error",
                                message: message,
                                type: "error",
                            });
                        });
                }
            });
        },
    },
};