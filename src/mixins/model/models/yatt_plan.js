import { mapGetters, mapActions } from "vuex";
import { regions } from "../../../utils/regions";

export default {
    data() {
        return {
            waiting: false,
            years: [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030],
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            rules: "yatt_plan/RULES",
            item: "yatt_plan/ITEM",
            items: "yatt_plan/ITEMS",
            columns: "yatt_plan/COLUMNS",
            getForm: "yatt_plan/FORM",
        }),
        all_regions() {
            let data = [];
            regions.forEach((element) => {
                let dataCh = [];
                element.under.forEach((elchild) => {
                    dataCh.push({
                        value: elchild.soato,
                        label: elchild.name_uz_cl,
                    });
                });

                data.push({
                    value: element.soato,
                    label: element.name_uz_cl,
                    children: dataCh,
                });
            });
            return data;
        },
    },
    async mounted() {
        this.list({ model: this.model_name, action_name: "list" }).then(() => {});
    },
    methods: {
        ...mapActions({}),
        setForm() {
            this.form = JSON.parse(JSON.stringify(this.getForm));
        },
        handleChangeProject(value) {
            this.form.soato = value[value.length - 1];
        },
        loadItem() {
            this.waiting = true;

            this.show({
                    model: this.model_name,
                    id: this.$route.params.id,
                    query: "",
                })
                .then(() => {
                    this.waiting = false;
                    let index = this.items.findIndex(
                        (element) => element.id == this.item.id
                    );
                    if (index > -1) {
                        this.items.splice(index, 1);
                    }
                    this.form = JSON.parse(JSON.stringify(this.item));
                })
                .catch(() => {
                    this.waiting = false;

                    this.$notify({
                        title: "Error",
                        message: "Ҳуқуқлар маълумотлари келмади.",
                        type: "error",
                    });
                });
        },
        onSave() {
            this.$refs["form"].validate((valid) => {
                if (valid) {
                    this.waiting = true;
                    this.save(this.form)
                        .then(() => {
                            this.waiting = false;
                            this.resetModel({ model: "company" });
                            this.setForm();
                            this.$router.push({
                                name: "YattPlanIndex",
                            });
                            this.$notify({
                                title: "Success",
                                message: "Маълумот сақланди.",
                                type: "success",
                            });
                        })
                        .catch((err) => {
                            this.waiting = false;
                            let message = "Маълумот сақлашда хатолик мавжуд.";
                            if (err.status == 409) {
                                message = "Бундай маълумот мавжуд.";
                            }
                            this.$notify({
                                title: "Error",
                                message: message,
                                type: "error",
                            });
                        });
                }
            });
        },
    },
};