import { mapGetters, mapActions } from "vuex";
export default {
    data() {
        return {
            waiting: false,
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            rules: "product/RULES",
            item: "product/ITEM",
            items: "product/ITEMS",
            columns: "product/COLUMNS",
            getForm: "product/FORM",
        }),
    },
    async mounted() {
        this.list({ model: this.model_name, action_name: "list" }).then(() => {});
    },
    methods: {
        ...mapActions({}),
        setForm() {
            this.form = JSON.parse(JSON.stringify(this.getForm));
        },
        loadItem() {
            this.waiting = true;

            this.show({
                    model: this.model_name,
                    id: this.$route.params.id,
                    query: "",
                })
                .then(() => {
                    this.waiting = false;
                    let index = this.items.findIndex(
                        (element) => element.id == this.item.id
                    );
                    if (index > -1) {
                        this.items.splice(index, 1);
                    }
                    this.form = JSON.parse(JSON.stringify(this.item));
                })
                .catch(() => {
                    this.waiting = false;

                    this.$notify({
                        title: "Error",
                        message: "Ҳуқуқлар маълумотлари келмади.",
                        type: "error",
                    });
                });
        },
        onSave() {
            this.$refs["form"].validate((valid) => {
                if (valid) {
                    this.waiting = true;
                    this.save(this.form)
                        .then(() => {
                            this.waiting = false;
                            this.resetModel({ model: "company" });
                            this.setForm();
                            this.$router.push({
                                name: "ProductIndex",
                            });
                            this.$notify({
                                title: "Success",
                                message: "Маълумот сақланди.",
                                type: "success",
                            });
                        })
                        .catch((err) => {
                            this.waiting = false;
                            let message = "Маълумот сақлашда хатолик мавжуд.";
                            if (err.status == 409) {
                                message = "Бундай маълумот мавжуд.";
                            }
                            this.$notify({
                                title: "Error",
                                message: message,
                                type: "error",
                            });
                        });
                }
            });
        },
    },
};