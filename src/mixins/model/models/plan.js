import { mapGetters, mapActions } from "vuex";
export default {
    data() {
        return {
            notFound: false,
            waiting: false,
            company: {
                name: "",
                tin: "",
            },
            model_name: "plan",
            years: [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030],
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            rules: "plan/RULES",
            item: "plan/ITEM",
            columns: "plan/COLUMNS",
            getForm: "plan/FORM",
            months: "plan/MONTHS",
            projects: "project/ITEMS",
            products: "product/ITEMS",
        }),
    },
    watch: {
        "form.company_tin": {
            handler: async function(newVal, oldVal) {
                if (
                    newVal &&
                    newVal.length == 9 &&
                    newVal != oldVal &&
                    !this.form.company &&
                    _.isFunction(this.debouncedFetchData)
                ) {
                    this.debouncedFetchData();
                }
            },
            deep: true,
            immediate: true,
        },
    },
    async created() {
        this.debouncedFetchData = _.debounce(this.getCompanyByTin, 100);
        this.list({ model: "project", action_name: "parents" }).then(() => {});
        this.list({ model: "product", action_name: "inventory" }).then(() => {});
    },
    async mounted() {},
    methods: {
        ...mapActions({
            getCompanyByTinAction: "company/getByTin",
        }),
        setForm() {
            this.form = JSON.parse(JSON.stringify(this.getForm));
        },
        resetForm() {
            this.resetModel({ model: "plan" });
            this.setForm();
        },
        getCompanyByTin() {
            this.waiting = true;
            this.getCompanyByTinAction({ tin: this.form.company_tin })
                .then((res) => {
                    this.waiting = false;
                    this.form.company = res.data;
                    this.notFound = false;
                })
                .catch((err) => {
                    this.notFound = true;
                    this.waiting = false;
                    this.$notify({
                        title: "Error",
                        message: "Mаълумотлари келмади.",
                        type: "error",
                    });
                });
        },
        loadItem() {
            this.waiting = true;

            this.show({
                    model: "plan",
                    id: this.$route.params.id,
                    query: "",
                })
                .then(() => {
                    this.waiting = false;

                    this.form = JSON.parse(JSON.stringify(this.item));
                })
                .catch(() => {
                    this.waiting = false;

                    this.$notify({
                        title: "Error",
                        message: "Ҳуқуқлар маълумотлари келмади.",
                        type: "error",
                    });
                });
        },
        handleChangeProject(value) {
            this.form.project_id = value[value.length - 1];
        },
        onSave() {
            this.$refs["form"].validate((valid) => {
                if (valid) {
                    this.waiting = true;
                    if (this.form.project_id != 2) {
                        this.form.product_id = null;
                        this.form.product_volume = null;
                    }

                    this.save(this.form)
                        .then(() => {
                            this.waiting = false;
                            this.resetModel({ model: "company" });
                            this.setForm();
                            this.$router.push({
                                name: "PlanIndex",
                            });
                            this.$notify({
                                title: "Success",
                                message: "Маълумот сақланди.",
                                type: "success",
                            });
                        })
                        .catch((err) => {
                            this.waiting = false;
                            let message = "Маълумот сақлашда хатолик мавжуд.";
                            if (err.status == 409) {
                                message = "Бундай маълумот мавжуд.";
                            }
                            this.$notify({
                                title: "Error",
                                message: message,
                                type: "error",
                            });
                        });
                }
            });
        },
    },
};