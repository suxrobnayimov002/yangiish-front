import { mapGetters, mapActions } from "vuex";
import { regions } from "../../../utils/regions";
export default {
    data() {
        return {
            waiting: false,
            data: [],
            soato: this.$route.params.soato,
        };
    },

    computed: {
        ...mapGetters({}),
        region() {
            return regions.find((region) => region.soato == this.soato.slice(0, 4));
        },
    },

    created() {
        this.waiting = true;
        this.getList();
    },
    methods: {
        ...mapActions({
            listAction: "plan/getCompaniesFullByMonthly",
        }),
        goBack() {
            this.$router.go(-1);
        },
        getList() {
            this.waiting = true;
            this.listAction({ soato: this.soato, project_id: this.project_id })
                .then((res) => {
                    this.data = res.data;
                    this.waiting = false;
                })
                .catch(() => {
                    this.waiting = false;
                });
        },
        handleChangeProject(value) {
            this.project_id = value[value.length - 1];
            this.getList();
        },
    },
};