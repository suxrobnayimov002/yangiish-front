import { mapGetters, mapActions } from "vuex";
import { regions } from "../../../utils/regions";
export default {
    data() {
        return {
            waiting: false,
            statuses: [
                { id: 0, name: "Фаол эмас" },
                { id: 1, name: "Фаол" },
            ],
            user_types: [
                { value: "user", name: "Фойдаланувчи" },
                { value: "operator", name: "Оператор" },
                { value: "admin", name: "Aдмин" },
                { value: "manager", name: "Менеджер" },
                { value: "viewer", name: "Кузатувчи" },
            ],
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            rules: "user/RULES",
            item: "user/ITEM",
            columns: "user/COLUMNS",
            getForm: "user/FORM",
            roles: "role/ITEMS",
        }),
        all_regions() {
            let data = [{
                value: "17",
                label: "Республика",
            }, ];
            regions.forEach((element) => {
                let dataCh = [];
                element.under.forEach((elchild) => {
                    dataCh.push({
                        value: elchild.soato,
                        label: elchild.name_uz_cl,
                    });
                });

                data.push({
                    value: element.soato,
                    label: element.name_uz_cl,
                    children: dataCh,
                });
            });
            return data;
        },
    },
    async mounted() {},
    async created() {
        this.getRoles();
    },
    methods: {
        ...mapActions({
            resetModel: "user/resetModel",
        }),
        setForm() {
            this.form = JSON.parse(JSON.stringify(this.getForm));
        },
        handleChangeProject(value) {
            this.form.soato = value[value.length - 1];
        },
        async getRoles() {
            await this.list({
                    model: "role",
                    action_name: "inventory",
                    query: "",
                })
                .then((res) => {})
                .catch((error) => {
                    this.$notify({
                        title: "Error",
                        message: "Ma'lumot olishda xatolik mavjud .",
                        type: "error",
                    });
                });
        },
        loadItem() {
            this.waiting = true;
            this.show({
                    model: "user",
                    id: this.$route.params.id,
                    query: "",
                })
                .then((res) => {
                    this.waiting = false;

                    this.form = JSON.parse(JSON.stringify(this.item));
                })
                .catch(() => {
                    this.waiting = false;

                    this.$notify({
                        title: "Error",
                        message: "Ҳуқуқлар маълумотлари келмади.",
                        type: "error",
                    });
                });
        },
        onSave() {
            this.$refs["form"].validate((valid) => {
                if (valid) {
                    this.waiting = true;
                    this.save(this.form)
                        .then((res) => {
                            this.waiting = false;
                            this.resetModel();
                            this.setForm();
                            this.$router.push({
                                name: "UserIndex",
                            });
                            this.$notify({
                                title: "Success",
                                message: "Маълумот сақланди.",
                                type: "success",
                            });
                        })
                        .catch((error) => {
                            this.waiting = false;
                            this.$notify({
                                title: "Error",
                                message: "Маълумот сақлашда хатолик мавжуд.",
                                type: "error",
                            });
                        });
                }
            });
        },
    },
};