import { mapGetters, mapActions } from "vuex";
export default {
    data() {
        return {
            waiting: false,
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            data: "plan/RECORD_PLAN",
            quarter_data: "plan/QUARTER_RECORD_POSITIONS",
            total: "plan/RECORD_PLAN_TOTAL",
            total_quarter: "plan/QUARTER_RECORD_POSITIONS_TOTAL",
        }),
        soato() {
            return this.$route.params.soato;
        },
    },
    watch: {
        quarter_month_from: {
            handler: async function(newVal, oldVal) {
                if (newVal != oldVal) {
                    this.getQuarterReportPositions();
                }
            },
            // deep: true,
            // immediate: true,
        },
        quarter_month_to: {
            handler: async function(newVal, oldVal) {
                if (newVal != oldVal) {
                    this.getQuarterReportPositions();
                }
            },
            // deep: true,
            // immediate: true,
        },
    },
    methods: {
        ...mapActions({
            reportBySoato: "plan/reportBySoato",
            fullReportBySoato: "plan/fullReportBySoato",
            reportPositionsQuarterBySoato: "plan/reportPositionsQuarterBySoato",
            getRegionReportPositionsFirst: "plan/getRegionReportPositionsFirst",
            getRegionReportPositionsSecond: "plan/getRegionReportPositionsSecond",
        }),
        projectChange(id) {
            this.waiting = true;
            this.getRegionReportPositionsFunc(id);
        },
        getReportData() {
            this.waiting = true;
            let a_soato = this.soato ? this.soato : "17";
            this.reportBySoato({ soato: a_soato })
                .then(() => {
                    this.waiting = false;
                })
                .catch(() => {
                    this.waiting = false;
                });
        },
        getFullReportData() {
            this.waiting = true;
            let a_soato = this.soato ? this.soato : "17";
            this.fullReportBySoato({ soato: a_soato })
                .then(() => {
                    this.waiting = false;
                })
                .catch(() => {
                    this.waiting = false;
                });
        },
        getRegionReportPositionsFunc(project_id) {
            this.waiting = true;
            let a_soato = this.soato ? this.soato : "17";
            if (project_id < 12) {
                this.getRegionReportPositionsFirst({
                        soato: a_soato,
                        project_id: project_id,
                    })
                    .then(() => {
                        this.waiting = false;
                    })
                    .catch(() => {
                        this.waiting = false;
                    });
            } else {
                this.getRegionReportPositionsSecond({
                        soato: a_soato,
                        project_id: project_id,
                    })
                    .then(() => {
                        this.waiting = false;
                    })
                    .catch(() => {
                        this.waiting = false;
                    });
            }
        },
        getQuarterReportPositions() {
            this.waiting = true;
            let quarter_month_arr = [this.quarter_month_from, this.quarter_month_to];
            let a_soato = this.soato ? this.soato : "17";
            this.reportPositionsQuarterBySoato({
                    soato: a_soato,
                    months: quarter_month_arr,
                })
                .then(() => {
                    this.waiting = false;
                })
                .catch(() => {
                    this.waiting = false;
                });
        },
    },
    //   async mounted() {
    //     if (this.$route.name != "Dashboard" && this.$route.name != "mapDashboard") {
    //       this.getReportData();
    //     } else {
    //       this.getQuarterReportPositions();
    //     }
    //   },
};