import { mapGetters, mapActions } from "vuex";
import { regions } from "@/utils/regions";
export default {
    data() {
        return {
            waiting: false,
            project_id: null,
            month: 12,
            type: "bar",
            series: [{
                    name: "Режада",
                    data: [],
                },
                {
                    name: "Aмалда",
                    data: [],
                },
            ],
            chartOptions: {
                chart: {
                    type: "bar",
                    height: 700,
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        dataLabels: {
                            position: "top",
                        },
                    },
                },
                dataLabels: {
                    enabled: true,
                    offsetX: -6,
                    style: {
                        fontSize: "12px",
                        colors: ["#fff"],
                    },
                },
                stroke: {
                    show: true,
                    width: 1,
                    colors: ["#fff"],
                },
                tooltip: {
                    shared: true,
                    intersect: false,
                },
                xaxis: {
                    categories: this.region_names,
                    position: "left",
                },
            },
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            statistic_plan: "plan/STATISTIC_PLAN",
            months: "plan/MONTHS",
        }),
        soato() {
            return this.$route.params.soato;
        },

        region_names() {
            let arr = [];
            let index = 1;
            let region_data = regions;

            if (this.soato) {
                region_data = regions.find((region) => region.soato == this.soato)
                    .under;
            }

            region_data.forEach((element) => {
                arr.push(index + ". " + element.name_uz_cl);
                index++;
            });
            return arr;
        },
    },
    watch: {
        project_id: {
            handler: async function(newVal, oldVal) {
                if (newVal != oldVal && _.isFunction(this.debouncedFetchData)) {
                    this.debouncedFetchData();
                }
            },
            deep: true,
            immediate: true,
        },
        month: {
            handler: async function(newVal, oldVal) {
                if (newVal != oldVal && _.isFunction(this.debouncedFetchData)) {
                    this.debouncedFetchData();
                }
            },
            deep: true,
            immediate: true,
        },
    },
    async mounted() {},
    async created() {
        var TodayDate = new Date();
        this.month = TodayDate.getMonth() + 1;
        this.project_id = 1;
        this.debouncedFetchData = _.debounce(this.getStatisticPlan, 100);
        this.chartOptions.xaxis.categories = this.region_names;
    },
    methods: {
        ...mapActions({
            statisticNewPositionsByProjectId: "plan/statisticNewPositionsByProjectId",
        }),

        setChartData() {
            this.series = [{
                    name: "Режада",
                    data: this.statistic_plan.plan,
                },
                {
                    name: "Aмалда",
                    data: this.statistic_plan.gnk,
                },
            ];
            this.chartOptions = {
                chart: {
                    type: "bar",
                    height: 900,
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        dataLabels: {
                            position: "top",
                        },
                    },
                },
                dataLabels: {
                    enabled: true,
                    offsetX: -6,
                    style: {
                        fontSize: "12px",
                        colors: ["#fff"],
                    },
                },
                stroke: {
                    show: true,
                    width: 1,
                    colors: ["#fff"],
                },
                tooltip: {
                    shared: true,
                    intersect: false,
                },
                xaxis: {
                    categories: this.region_names,
                },

                yaxis: {
                    categories: this.region_names,
                    position: "left",
                    labels: {
                        align: "left",
                        offsetX: 0,
                        minWidth: 220,
                        maxWidth: 220,
                        text: undefined,
                        offsetX: 0,
                        offsetY: 0,
                        style: {
                            color: undefined,
                            fontSize: "12px",
                            fontWeight: 400,
                        },
                    },
                },
            };
        },
        getStatisticPlan() {
            // this.waiting = true;
            let a_soato = this.soato ? this.soato : "17";
            this.statisticNewPositionsByProjectId({
                    soato: a_soato,
                    project_id: this.project_id,
                    // month: this.month,
                })
                .then(() => {
                    // this.waiting = false;
                    this.setChartData();
                })
                .catch(() => {
                    // this.waiting = false;
                });
        },
    },
};