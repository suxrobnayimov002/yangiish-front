import { mapGetters, mapActions } from "vuex";
export default {
    data() {
        return {
            pieSeries: [1, 1, 1, 1, 1],
            pieChartOptions: {
                chart: {
                    width: 1000,
                    type: "pie",
                },
                dataLabels: {
                    // enabled: false,
                },
                labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200,
                        },
                        legend: {
                            show: false,
                        },
                    },
                }, ],
            },
        };
    },
    components: {},
    computed: {
        ...mapGetters({
            total_quarter: "plan/QUARTER_RECORD_TOTAL_FULL_PERSONS",
        }),
        soato() {
            return this.$route.params.soato;
        },
    },
    watch: {
        // total_quarter: {
        //     handler: async function(newVal, oldVal) {
        //         if (newVal != oldVal) {
        //             this.setPieChartData();
        //         }
        //     },
        //     deep: true,
        //     immediate: true,
        // },
    },
    async mounted() {},
    async created() {
        this.getTotalDataFunc();
    },
    methods: {
        ...mapActions({
            getTotalData: "plan/reportPositionsQuarterTotalBySoato",
        }),
        getTotalDataFunc() {
            let quarter_month_arr = [1, 12];
            let a_soato = this.soato ? this.soato : "17";
            this.getTotalData({
                    soato: a_soato,
                    months: quarter_month_arr,
                })
                .then(() => {
                    this.setPieChartData();
                })
                .catch(() => {});
        },
        getCharItemData(p_positions, e_positions) {
            let a = p_positions - (e_positions > 0 ? e_positions : 0);
            a = a > 0 ? a : 1;
            return a;
        },
        setPieChartData() {
            // this.pieSeriesArr = [];
            if (this.total_quarter) {
                this.pieSeries[0] = this.getCharItemData(
                    this.total_quarter.positions_count_p2,
                    this.total_quarter.enst_positions_count_p2
                );
                this.pieSeries[1] = this.getCharItemData(
                    this.total_quarter.positions_count_p3,
                    this.total_quarter.enst_positions_count_p3
                );
                this.pieSeries[2] = this.getCharItemData(
                    this.total_quarter.positions_count_p7,
                    this.total_quarter.enst_positions_count_p7
                );
                this.pieSeries[3] = this.getCharItemData(
                    this.total_quarter.positions_count_p13,
                    this.total_quarter.enst_positions_count_p13
                );
                this.pieSeries[4] = this.getCharItemData(
                    this.total_quarter.positions_count_p14,
                    this.total_quarter.enst_positions_count_p14
                );
            }
            // this.pieSeries[0]
            this.pieChartOptions = {
                chart: {
                    width: 1000,
                    type: "pie",
                },
                labels: [
                    "Тармоқ инвестиция лойиҳаларини амалга ошириш",
                    "Ҳудудий инвестиция лойиҳаларини амалга ошириш",
                    "Ижтимоий инфратузилмани ривожлантириш",
                    "Янги кичик корхона ва микрофирмалар ташкил этиш ҳисобига",
                    "Якка тартибдаги тадбиркорликни, шу жумладан ҳунармандчиликни ривожлантириш",
                ],
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200,
                        },
                        legend: {
                            position: "bottom",
                        },
                    },
                }, ],
            };
        },
    },
};