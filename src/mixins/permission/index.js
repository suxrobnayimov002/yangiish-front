import { mapActions, mapGetters } from "vuex";

export default {
    data() {
        return {
            loadData: false,
        };
    },
    computed: {
        ...mapGetters({
            role: "auth/role",
        }),
    },
    created() {},
    methods: {
        ...mapActions({
            getInfo: "auth/getInfo",
        }),
        can(slug) {
            // if (!this.role && !this.loadData) {
            //     this.loadData = true;
            //     this.getInfoFunction();
            // }
            // if (!this.role ||
            //     !this.role ||
            //     !this.role.permissions ||
            //     this.role.permissions.length == 0
            // ) {
            //     return false;
            // }
            // if (
            //     this.role.permissions.some((item) => item == "*") ||
            //     this.role.permissions.some((item) => item.slug == slug) ||
            //     this.role.permissions.some((item) => item.slug == "*")
            // ) {
            //     return true;
            // }
            return true;
        },
        async getInfoFunction() {
            await this.getInfo();
        },
    },
};