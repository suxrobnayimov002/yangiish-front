import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const yakkPlanRoutes = {
    path: "/yakk_plans",
    component: MainLayout,
    redirect: "/yakk_plans/list",
    children: [{
            path: "/yakk_plans/list",
            component: () =>
                import ("@/views/YakkPlan"),
            name: "YakkPlanIndex",
            meta: {
                permission: "yakk_plans.index",
                title: "Барча режалар",
                middleware: [auth],
            },
        },
        {
            path: "/yakk_plans/create",
            component: () =>
                import ("@/views/YakkPlan/Create"),
            name: "YakkPlanStore",
            meta: {
                title: "Режа қўшиш",
                permission: "yakk_plans.create",
                middleware: [auth],
            },
        },
        {
            path: "/yakk_plans/update/:id",
            component: () =>
                import ("@/views/YakkPlan/Update"),
            name: "YakkPlanUpdate",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};