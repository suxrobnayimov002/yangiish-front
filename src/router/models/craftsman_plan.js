import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const craftsmanPlanRoutes = {
    path: "/craftsman_plans",
    component: MainLayout,
    redirect: "/craftsman_plans/list",
    children: [{
            path: "/craftsman_plans/list",
            component: () =>
                import ("@/views/CraftsmanPlan"),
            name: "CraftsmanPlanIndex",
            meta: {
                permission: "craftsman_plans.index",
                title: "Барча режалар",
                middleware: [auth],
            },
        },
        {
            path: "/craftsman_plans/create",
            component: () =>
                import ("@/views/CraftsmanPlan/Create"),
            name: "CraftsmanPlanStore",
            meta: {
                title: "Режа қўшиш",
                permission: "craftsman_plans.create",
                middleware: [auth],
            },
        },
        {
            path: "/craftsman_plans/update/:id",
            component: () =>
                import ("@/views/CraftsmanPlan/Update"),
            name: "CraftsmanPlanUpdate",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};