import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const reportRoutes = {
    path: "/reports",
    component: MainLayout,
    redirect: "/dashboard",
    children: [{
            path: "/reports/statistic-by-soato/:soato",
            component: () =>
                import ("@/views/Report/statisticBySoato"),
            name: "StatisticBySoato",
            meta: {
                permission: "reports.statisticBySoato",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/by-districts/:soato",
            component: () =>
                import ("@/views/Report/byDistricts"),
            name: "ReportByDistricts",
            meta: {
                permission: "reports.byDistrict",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/sec-by-districts/:soato",
            component: () =>
                import ("@/views/Report/SecByDistricts"),
            name: "ReportSecByDistricts",
            meta: {
                permission: "reports.secByDistrict",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/:project_id/district/social/:soato",
            component: () =>
                import ("@/views/Report/social"),
            name: "ReportSocial",
            meta: {
                permission: "reports.secSocial",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/district/yatt/:soato",
            component: () =>
                import ("@/views/Report/yatt"),
            name: "ReportYatt",
            meta: {
                permission: "reports.yatt",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/districts/:soato/directions/:type_id",
            component: () =>
                import ("@/views/Report/District/byDirections"),
            name: "ReportByDirections",
            meta: {
                permission: "reports.byDirections",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/districts/:soato/social/:type_id",
            component: () =>
                import ("@/views/Report/Social/bySocial"),
            name: "ReportBySocial",
            meta: {
                permission: "reports.bySocial",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/investment/:soato",
            component: () =>
                import ("@/views/Report/investment"),
            name: "ReportInvestment",
            meta: {
                permission: "reports.investment",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/reports/area-compare/:soato",
            component: () =>
                import ("@/views/Report/compareByRegion"),
            name: "CompareArea",
            meta: {
                permission: "reports.compare",
                title: "",
                middleware: [auth],
            },
        },
        {
            path: "/reports/full-by-soato/:soato",
            component: () =>
                import ("@/views/Report/fullByRegion"),
            name: "FullByRegion",
            meta: {
                permission: "reports.full_data",
                title: "",
                middleware: [auth],
            },
        },
        {
            path: "/reports/companies-full-by-monthly/:soato",
            component: () =>
                import ("@/views/Report/companiesFullByMonthly"),
            name: "CompaniesFullByMonthly",
            meta: {
                permission: "reports.compare",
                title: "",
                middleware: [auth],
            },
        },
        {
            path: "/reports/compare-companies-monthly-by-soato/:soato",
            component: () =>
                import ("@/views/Report/compareCurrentMonth"),
            name: "CompareCurrentMonth",
            meta: {
                permission: "reports.compare",
                title: "",
                middleware: [auth],
            },
        },
        {
            path: "/reports/companies-vacancies-count-by-soato/:soato",
            component: () =>
                import ("@/views/Report/withVacanciesCount"),
            name: "WithVacanciesCount",
            meta: {
                permission: "reports.compare",
                title: "",
                middleware: [auth],
            },
        },
    ],
};