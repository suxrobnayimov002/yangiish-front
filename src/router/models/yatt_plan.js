import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const yattPlanRoutes = {
    path: "/yatt_plans",
    component: MainLayout,
    redirect: "/yatt_plans/list",
    children: [{
            path: "/yatt_plans/list",
            component: () =>
                import ("@/views/YattPlan"),
            name: "YattPlanIndex",
            meta: {
                permission: "yatt_plans.index",
                title: "Барча режалар",
                middleware: [auth],
            },
        },
        {
            path: "/yatt_plans/create",
            component: () =>
                import ("@/views/YattPlan/Create"),
            name: "YattPlanStore",
            meta: {
                title: "Режа қўшиш",
                permission: "yatt_plans.create",
                middleware: [auth],
            },
        },
        {
            path: "/yatt_plans/update/:id",
            component: () =>
                import ("@/views/YattPlan/Update"),
            name: "YattPlanUpdate",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};