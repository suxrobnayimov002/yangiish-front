import MainLayout from "@/layouts/main";
import checkPermission from "../middlewares/checkPermission";
export const userRoutes = {
    path: "/users",
    component: MainLayout,
    redirect: "/users/list",
    children: [{
            path: "/users/list",
            component: () =>
                import ("@/views/User"),
            name: "UserIndex",
            meta: {
                permission: "user.index",
                title: "Фойдаланувчилар",
                middleware: [checkPermission],
            },
        },
        {
            path: "/users/create",
            component: () =>
                import ("@/views/User/Create"),
            name: "UserStore",
            meta: {
                permission: "user.create",
                title: "Фойдаланувчи қўшиш",
                middleware: [checkPermission],
            },
        },
        {
            path: "/users/update/:id",
            component: () =>
                import ("@/views/User/Update"),
            name: "UserUpdate",
            meta: {
                title: "Фойдаланувчини ўзгартириш",
                middleware: [checkPermission],
            },
        },
    ],
};