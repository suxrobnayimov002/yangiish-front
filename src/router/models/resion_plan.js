import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const regionPlanRoutes = {
    path: "/region_plans",
    component: MainLayout,
    redirect: "/region_plans/list",
    children: [{
            path: "/region_plans/list",
            component: () =>
                import ("@/views/RegionPlan"),
            name: "RegionPlanIndex",
            meta: {
                permission: "region_plans.index",
                title: "Барча режалар",
                middleware: [auth],
            },
        },
        {
            path: "/region_plans/create",
            component: () =>
                import ("@/views/RegionPlan/Create"),
            name: "RegionPlanStore",
            meta: {
                title: "Режа қўшиш",
                permission: "region_plans.create",
                middleware: [auth],
            },
        },
        {
            path: "/region_plans/update/:id",
            component: () =>
                import ("@/views/RegionPlan/Update"),
            name: "RegionPlanUpdate",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};