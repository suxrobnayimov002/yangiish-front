import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const companyRoutes = {
    path: "/companies",
    component: MainLayout,
    redirect: "/companies/list",
    children: [{
            path: "/companies/list",
            component: () =>
                import ("@/views/Company"),
            name: "CompanyIndex",
            meta: {
                permission: "companies.index",
                title: "Барча корхоналар",
                middleware: [auth],
            },
        },
        {
            path: "/companies/create",
            component: () =>
                import ("@/views/Company/Create"),
            name: "CompanyStore",
            meta: {
                title: "Корхона қўшиш",
                permission: "companies.create",
                middleware: [auth],
            },
        },
        {
            path: "/companies/update/:tin",
            component: () =>
                import ("@/views/Company/Update"),
            name: "CompanyUpdate",
            meta: {
                title: "Корхонани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
        {
            path: "/companies/has-not-enst",
            component: () =>
                import ("@/views/Company/hasNotEnst"),
            name: "CompanyHasNotEnst",
            meta: {
                // title: "Корхонани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};