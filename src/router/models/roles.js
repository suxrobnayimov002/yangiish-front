import MainLayout from "@/layouts/main";
import checkPermission from "../middlewares/checkPermission";
export const roleRoutes = {
    path: "/roles",
    component: MainLayout,
    redirect: "/roles/list",
    children: [{
            path: "/roles/list",
            component: () =>
                import ("@/views/Roles"),
            name: "Roles",
            meta: {
                title: "Роллар",
                permission: "role.index",
                middleware: [checkPermission],
            },
        },
        {
            path: "/roles/create",
            component: () =>
                import ("@/views/Roles/Create"),
            name: "RoleStore",
            meta: {
                title: "Рол қўшиш",
                permission: "role.create",
                middleware: [checkPermission],
            },
        },
        {
            path: "/roles/update/:id",
            component: () =>
                import ("@/views/Roles/Update"),
            name: "RoleUpdate",
            meta: {
                title: "Ролни ўзгартириш",
                permission: "role.update",
                middleware: [checkPermission],
            },
        },
    ],
};