import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const productRoutes = {
    path: "/products",
    component: MainLayout,
    redirect: "/products/list",
    children: [{
            path: "/products/list",
            component: () =>
                import ("@/views/Product"),
            name: "ProductIndex",
            meta: {
                permission: "products.index",
                title: "Барча режалар",
                middleware: [auth],
            },
        },
        {
            path: "/products/create",
            component: () =>
                import ("@/views/Product/Create"),
            name: "ProductStore",
            meta: {
                title: "Режа қўшиш",
                permission: "products.create",
                middleware: [auth],
            },
        },
        {
            path: "/products/update/:id",
            component: () =>
                import ("@/views/Product/Update"),
            name: "ProductUpdate",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};