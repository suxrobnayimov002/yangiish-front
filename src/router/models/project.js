import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const projectRoutes = {
    path: "/projects",
    component: MainLayout,
    redirect: "/projects/list",
    children: [{
            path: "/projects/list",
            component: () =>
                import ("@/views/Project"),
            name: "ProjectIndex",
            meta: {
                permission: "projects.index",
                title: "Барча лойиҳалар",
                middleware: [auth],
            },
        },
        {
            path: "/projects/create",
            component: () =>
                import ("@/views/Project/Create"),
            name: "ProjectStore",
            meta: {
                title: "Лойиҳа қўшиш",
                permission: "projects.create",
                middleware: [auth],
            },
        },
        {
            path: "/projects/update/:id",
            component: () =>
                import ("@/views/Project/Update"),
            name: "ProjectUpdate",
            meta: {
                title: "Лойиҳани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};