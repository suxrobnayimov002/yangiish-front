import MainLayout from "@/layouts/main";
import auth from "@/router/middlewares/auth";
export const planRoutes = {
    path: "/plans",
    component: MainLayout,
    redirect: "/plans/list",
    children: [{
            path: "/plans/list",
            component: () =>
                import ("@/views/Plan"),
            name: "PlanIndex",
            meta: {
                permission: "plans.index",
                title: "Барча режалар",
                middleware: [auth],
            },
        },
        {
            path: "/plans/create",
            component: () =>
                import ("@/views/Plan/Create"),
            name: "PlanStore",
            meta: {
                title: "Режа қўшиш",
                permission: "plans.create",
                middleware: [auth],
            },
        },
        {
            path: "/plans/update/:id",
            component: () =>
                import ("@/views/Plan/Update"),
            name: "PlanUpdate",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
        {
            path: "/plans/has-not-info-gnk",
            component: () =>
                import ("@/views/Plan/hasNotInfoGnk"),
            name: "HasNotInfoGnk",
            meta: {
                title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
        {
            path: "/plans/duplicate-companies",
            component: () =>
                import ("@/views/Plan/duplicateCompanies"),
            name: "DuplicateCompanies",
            meta: {
                // title: "Режани ўзгартириш",
                //permission: "methods.update",
                middleware: [auth],
            },
        },
    ],
};