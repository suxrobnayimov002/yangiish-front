import store from "@/store";
export default async function checkPermission({ next, router, to }) {
    let result = await can(to.meta.permission);
    if (!result) {
        return next({
            name: "error-403",
        });
    }
    return next();
}

async function can(slug) {
    return true;
    // console.log(store.getters["auth/role"]);
    if (!store.getters["auth/role"]) {
        await getInfoFunction();
    }
    if (!store.getters["auth/role"] ||
        !store.getters["auth/role"].permissions ||
        store.getters["auth/role"].permissions.length == 0
    ) {
        return false;
    }
    if (
        store.getters["auth/role"].permissions.some((item) => item.slug == slug) ||
        store.getters["auth/role"].permissions.some((item) => item.slug == "*") ||
        store.getters["auth/role"].permissions.some((item) => item == "*")
    ) {
        return true;
    }
    return false;
}
async function getInfoFunction() {
    await store.dispatch("auth/getInfo");
}