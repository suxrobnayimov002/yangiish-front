import { getAccessToken } from '@/utils/auth' // get token from cookie

export default function auth({ to, from, next, store }) {
  const hasToken = getAccessToken()
  if (to.name === 'Login' && ((hasToken === undefined) || (hasToken === ''))) {
    return next()
  }
  if (hasToken) {
    return next()
    // store.dispatch('auth/getInfo')
    //     .then(() => {
    //       if (to.name === 'Login') {
    //         return next({ name: 'ClientIndex' })
    //       }
    //       return next()
    //     })
    //     .catch(err => {
    //       console.log(err)
    //       store.dispatch('auth/resetToken')
    //       next({ name: 'Login' })
    //     })
  } else {
    return next({ name: 'Login' })
  }
}
