import Vue from "vue";
import VueRouter from "vue-router";
import MainLayout from "@/layouts/main";
import { planRoutes } from "./models/plan";
import { projectRoutes } from "./models/project";
import { companyRoutes } from "./models/company";
import { productRoutes } from "./models/product";
import { reportRoutes } from "./models/report";
import { userRoutes } from "./models/user";
import { regionPlanRoutes } from "./models/resion_plan";
import { yattPlanRoutes } from "./models/yatt_plan";
import { yakkPlanRoutes } from "./models/yakk_plan";
import { craftsmanPlanRoutes } from "./models/craftsman_plan";
Vue.use(VueRouter);

const routes = [{
        name: "AuthToken",
        path: "/auth-token",
        component: () =>
            import ("@/views/Auth/withToken"),
    },
    {
        path: "/auth/login",
        component: () =>
            import ("../views/Auth/oneid/loginOneId"),
        name: "Login",
        meta: {
            title: "Кириш",
        },
    },
    {
        path: "/auth/admin/login",
        component: () =>
            import ("../views/Auth/login"),
        name: "Login",
        meta: {
            title: "Кириш",
        },
    },
    {
        path: "/auth/oneid",
        name: "LoginViaOneid",
        component: () =>
            import ("@/views/Auth/oneid/code"),
    },
    {
        path: "/",
        component: MainLayout,
        redirect: "/dashboard-map",
        children: [{
                path: "/dashboard-map",
                component: () =>
                    import ("../views/dashboard/mapDashboard"),
                name: "mapDashboard",
                meta: {
                    title: "Map Dashboard",
                },
            },
            {
                path: "/dashboard",
                component: () =>
                    import ("../views/dashboard/Dashboard"),
                name: "Dashboard",
                meta: {
                    title: "Dashboard",
                },
            },
            {
                path: "/dashboard-test",
                component: () =>
                    import ("../views/DashboardTest"),
                name: "DashboardTest",
                meta: {
                    title: "Dashboard",
                },
            },
        ],
    },
    planRoutes,
    projectRoutes,
    companyRoutes,
    productRoutes,
    reportRoutes,
    userRoutes,
    regionPlanRoutes,
    yattPlanRoutes,
    yakkPlanRoutes,
    craftsmanPlanRoutes,
    {
        path: "/error-403",
        component: () =>
            import ("../views/403"),
        name: "error-403",
        meta: {
            title: "403",
        },
    },
    {
        path: "*",
        component: () =>
            import ("../views/404"),
        name: "error-404",
        meta: {
            title: "404",
        },
    },
];

const createRouter = () =>
    new VueRouter({
        mode: "history",
        scrollBehavior: () => ({
            y: 0,
        }),
        routes,
    });
const router = createRouter();

function nextFactory(context, middleware, index) {
    const subsequentMiddleware = middleware[index];
    if (!subsequentMiddleware) return context.next;

    return (...parameters) => {
        context.next(...parameters);
        const nextMiddleware = nextFactory(context, middleware, index + 1);
        subsequentMiddleware({
            ...context,
            next: nextMiddleware,
        });
    };
}

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware) ?
            to.meta.middleware :
            [to.meta.middleware];
        const context = {
            from,
            next,
            router,
            to,
        };
        const nextMiddleware = nextFactory(context, middleware, 1);

        return middleware[0]({
            ...context,
            next: nextMiddleware,
        });
    }
    return next();
});
export function resetRouter() {
    const newRouter = createRouter();
    router.matcher = newRouter.matcher; // reset router
}

export default router;