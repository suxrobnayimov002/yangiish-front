"use strict";
const path = require("path");

function resolve(dir) {
    return path.join(__dirname, dir);
}

const name = require("./package.json").name;

const appVersion = require("./package.json").version;

const buildTime = new Date().getTime();

const cssConfig = {};

if (process.env.NODE_ENV === "production") {
    cssConfig.extract = {
        filename: `css/[name].[contenthash:8].${buildTime}.${appVersion}.css`,
        chunkFilename: `css/[name].[contenthash:8].${buildTime}.${appVersion}.css`,
    };
}
module.exports = {
    publicPath: "/",
    outputDir: "dist",
    assetsDir: "static",
    lintOnSave: process.env.NODE_ENV === "development",
    productionSourceMap: false,
    configureWebpack: {
        name: name,
        resolve: {
            alias: {
                "@": resolve("src"),
            },
        },
    },
    css: cssConfig,
    chainWebpack(config) {
        config.when(process.env.NODE_ENV === "production", (config) =>
            config.output
            .filename(`js/[name].[contenthash:8].${buildTime}.${appVersion}.js`)
            .chunkFilename(
                `js/[name].[contenthash:8].${buildTime}.${appVersion}.js`
            )
        );
    },
};